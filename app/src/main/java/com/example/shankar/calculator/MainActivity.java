package com.example.shankar.calculator;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    double d[] = new double[1000];
    char action[] = new char[1000];
    int i = 0, j = 0;
    MediaPlayer mediaPlayer = new MediaPlayer();


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        mediaPlayer = mediaPlayer.create(MainActivity.this, R.raw.song);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b19, b20, b21, b22, b23, b24, b25, b26, b27, b34;
        b1 = (Button) findViewById(R.id.button);
        b2 = (Button) findViewById(R.id.button2);
        b3 = (Button) findViewById(R.id.button3);
        b4 = (Button) findViewById(R.id.button4);
        b5 = (Button) findViewById(R.id.button5);
        b6 = (Button) findViewById(R.id.button6);
        b7 = (Button) findViewById(R.id.button7);
        b8 = (Button) findViewById(R.id.button8);
        b9 = (Button) findViewById(R.id.button9);
        b10 = (Button) findViewById(R.id.button10);
        b11 = (Button) findViewById(R.id.button11);
        b12 = (Button) findViewById(R.id.button12);
        b13 = (Button) findViewById(R.id.button13);
        b14 = (Button) findViewById(R.id.button14);
        b15 = (Button) findViewById(R.id.button15);
        b16 = (Button) findViewById(R.id.button16);
        b17 = (Button) findViewById(R.id.button17);
        b18 = (Button) findViewById(R.id.button18);
        b19 = (Button) findViewById(R.id.button19);
        b20 = (Button) findViewById(R.id.button20);
        b21 = (Button) findViewById(R.id.button21);
        b22 = (Button) findViewById(R.id.button22);
        b23 = (Button) findViewById(R.id.button23);
        b24 = (Button) findViewById(R.id.button24);
        b25 = (Button) findViewById(R.id.button25);
        b26 = (Button) findViewById(R.id.button26);
        b27 = (Button) findViewById(R.id.button27);
        b34 = (Button) findViewById(R.id.button34);
        final EditText ed = (EditText) findViewById(R.id.editText);
        ed.setText("0");
        ed.setInputType(0);
        b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                            ed.setText("1");
                        } else {

                            ed.setText(ed.getText() + "1");
                        }


            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("2");
                } else {
                    ed.setText(ed.getText() + "2");
                }

            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("3");
                } else {
                    ed.setText(ed.getText() + "3");
                }

            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("4");
                } else {
                    ed.setText(ed.getText() + "4");
                }
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("5");
                } else {
                    ed.setText(ed.getText() + "5");
                }
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("6");
                } else {
                    ed.setText(ed.getText() + "6");
                }
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("7");
                } else {
                    ed.setText(ed.getText() + "7");
                }

            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("8");
                } else {
                    ed.setText(ed.getText() + "8");
                }

            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("9");
                } else {
                    ed.setText(ed.getText() + "9");
                }

            }
        });
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ed.getText() + "").charAt(0) == '0' && (ed.getText() + "").length() == 1) {
                    ed.setText("0");
                } else {
                    ed.setText(ed.getText() + "0");
                }

            }
        });
        b11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d[i] = Double.parseDouble(ed.getText() + "");
                i = i + 1;
                action[j] = 'M';
                j = j + 1;
                ed.setText("0");
            }
        });
        b12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d[i] = Double.parseDouble(ed.getText() + "");
                i = i + 1;
                action[j] = 'S';
                j = j + 1;
                ed.setText("0");
            }
        });
        b13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d[i] = Double.parseDouble(ed.getText() + "");
                ed.setText("0");
                i = i + 1;
                action[j] = 'D';
                j = j + 1;
            }
        });
        b14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d[i] = Double.parseDouble(ed.getText() + "");
                i = i + 1;
                action[j] = 'A';
                j = j + 1;
                ed.setText("0");
            }
        });
        b15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed.setText("0");
                d = new double[100];
                action = new char[100];


            }
        });
        b19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double x = 0;
                x = Double.parseDouble(ed.getText() + "");
                x = x * x;
                ed.setText(x + "");
                correct();
            }
        });
        b20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double x = 0;
                x = Double.parseDouble(ed.getText() + "");
                x = x * x * x;
                ed.setText(x + "");
                correct();
            }
        });
        b23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double x = 0;
                x = Double.parseDouble(ed.getText() + "");
                x = Math.sqrt(x);
                ed.setText(x + "");
                correct();
            }
        });
        b24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double x = 0;
                x = Double.parseDouble(ed.getText() + "");
                x = Math.cbrt(x);
                ed.setText(x + "");
                correct();
            }
        });
        b25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double x = 0;
                x = Double.parseDouble(ed.getText() + "");
                x = Math.sin(x);
                ed.setText(x + "");
                correct();
            }
        });
        b26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double x = 0;
                x = Double.parseDouble(ed.getText() + "");
                x = Math.cos(x);
                ed.setText(x + "");
                correct();
            }
        });
        b27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double x = 0;
                x = Double.parseDouble(ed.getText() + "");
                x = Math.tan(x);
                ed.setText(x + "");
                correct();
            }
        });
        b16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((ed.getText() + "").charAt(0) == 'E') {
                    ed.setText("0");
                } else {
                    d[i] = Double.parseDouble(ed.getText() + "");
                    double x = 0;
                    for (int m = 0; m < j; ++m) {

                        if (action[m] == 'A') {
                            x = d[m] + d[m + 1];
                            d[m + 1] = x;
                            ed.setText(x + "");
                        } else if (action[m] == 'S') {
                            x = d[m] - d[m + 1];
                            d[m + 1] = x;
                            ed.setText(x + "");
                        } else if (action[m] == 'M') {
                            x = d[m] * d[m + 1];
                            d[m + 1] = x;
                            ed.setText(x + "");
                        } else if (action[m] == 'D') {
                            if (d[m] != 0 && d[m + 1] != 0) {
                                x = d[m] / d[m + 1];
                                d[m + 1] = x;
                                ed.setText(x + "");
                            } else {
                                ed.setText("E");
                            }
                        } else {
                            ed.setText(d[i] + "");
                        }
                        correct();


                    }

                    d = new double[1000];
                    action = new char[1000];
                    i = 0;
                    j = 0;
                }
            }

        });

        b17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String x = ed.getText() + "";
                try {
                    x = x.substring(0, x.length() - 1);
                } catch (StringIndexOutOfBoundsException e) {
                    ed.setText("0");
                }
                if (x.length() == 0) {
                    ed.setText("0");
                } else {

                    ed.setText(x);
                }


            }
        });
        b18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ed.setText(ed.getText() + ".");


            }
        });


        b34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                } else {
                    mediaPlayer.start();
                }


            }
        });


    }

    public void correct() {
        final EditText ed = (EditText) findViewById(R.id.editText);
        double y = Double.parseDouble(ed.getText() + "");
        double x = y;
        y = y % 1;
        if (y == 0) {
            int z = 0;
            z = (int) x;
            ed.setText(z + "");
        }
    }
}
